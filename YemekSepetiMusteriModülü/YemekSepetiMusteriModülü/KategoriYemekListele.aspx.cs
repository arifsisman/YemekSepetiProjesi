﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class KategoriYemekListele : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {  
                DataSet kategoriler = KategoriCek();
                DropDownList1.DataTextField = "KategoriAdi";
                DropDownList1.DataValueField = "KategoriID";
                DropDownList1.DataSource = kategoriler.Tables[0];
                DropDownList1.DataBind();
                DropDownList1.SelectedIndex = 1;//
                DataSet yemekler = YemekCek();//
                GridView1.DataSource = yemekler.Tables[0];//
                GridView1.DataBind();//
                if (Request.QueryString["KategoriID"] != null)
                {
                    DropDownList1.SelectedValue = Request.QueryString["KategoriID"];
                    DataSet yemek = YemekCekQuery();
                    GridView1.DataSource = yemek.Tables[0];
                    GridView1.DataBind();
                }
            }

        }
        else
            Response.Redirect("Giris.aspx");
    }


    DataSet KategoriCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select KategoriAdi,KategoriID from Kategori order by KategoriAdi";//distinct i çıkardım
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet kategoriler = new DataSet();
        adaptor.Fill(kategoriler);

        return kategoriler;
    }

    DataSet YemekCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select Restoran.RestoranID,Restoran.Adi as RestoranAdi,YemekID,Yemek.YemekAdi,Yemek.HazirlanmaSuresi,Yemek.Aciklama,Yemek.Fiyat,Yemek.Resmi from Restoran,Yemek where Yemek.RestoranID = Restoran.RestoranID and Yemek.KategoriID=" + DropDownList1.SelectedValue;
        //komut.CommandText = "select Restoran.RestoranID,Restoran.Adi as RestoranAdi,Yemek.YemekID,Yemek.YemekAdi,Yemek.HazirlanmaSuresi,Yemek.Aciklama,Yemek.Fiyat,Yemek.Resmi,OpsiyonAdi,YemekOpsiyonuID from Opsiyon,Restoran,Yemek,YemekOpsiyonu where Yemek.YemekID=YemekOpsiyonu.YemekID and Opsiyon.OpsiyonID=YemekOpsiyonu.OpsiyonID and Yemek.RestoranID = Restoran.RestoranID and Yemek.KategoriID=" + DropDownList1.SelectedValue;


        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet donenVeri = new DataSet();
        adaptor.Fill(donenVeri);
        return donenVeri;
    }

    DataSet YemekCekQuery()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select Restoran.RestoranID,Restoran.Adi as RestoranAdi,YemekID,Yemek.YemekAdi,Yemek.HazirlanmaSuresi,Yemek.Aciklama,Yemek.Fiyat,Yemek.Resmi from Restoran,Yemek where Yemek.RestoranID = Restoran.RestoranID and Yemek.KategoriID=" + Request.QueryString["KategoriID"];
        //komut.CommandText = "select Restoran.RestoranID,Restoran.Adi as RestoranAdi,Yemek.YemekID,Yemek.YemekAdi,Yemek.HazirlanmaSuresi,Yemek.Aciklama,Yemek.Fiyat,Yemek.Resmi,OpsiyonAdi,YemekOpsiyonuID from Opsiyon,Restoran,Yemek,YemekOpsiyonu where Yemek.YemekID=YemekOpsiyonu.YemekID and Opsiyon.OpsiyonID=YemekOpsiyonu.OpsiyonID and Yemek.RestoranID = Restoran.RestoranID and Yemek.KategoriID=" + DropDownList1.SelectedValue;


        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet donenVeri = new DataSet();
        adaptor.Fill(donenVeri);
        return donenVeri;
    }


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet yemekler = YemekCek();
        GridView1.DataSource = yemekler.Tables[0];
        GridView1.DataBind();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "SepeteEkle")
        //{
        //    int satirIndeksi = Convert.ToInt32(e.CommandArgument);
        //    GridViewRow satir = GridView1.Rows[satirIndeksi];
        //    string yemekID = ((Label)satir.FindControl("Label21")).Text;
        //    string restID= ((Label)satir.FindControl("Label22")).Text;
        //    string adet = ((TextBox)satir.FindControl("txtAdet")).Text;
        //    string fiyat= ((Label)satir.FindControl("Label16")).Text;
        //    string ops1="",ops2="",ops3="";
        //    if (((CheckBox)satir.FindControl("CheckBox1")).Checked)
        //        ops1 = ((Label)satir.FindControl("Label17")).Text;
        //    if (((CheckBox)satir.FindControl("CheckBox2")).Checked)
        //        ops2 = ((Label)satir.FindControl("Label18")).Text;
        //    if (((CheckBox)satir.FindControl("CheckBox3")).Checked)
        //        ops3 = ((Label)satir.FindControl("Label19")).Text;

        //    //Response.Write("yemekID="+yemekID+"adet= "+adet);
        //    SepeteEkle(restID,yemekID,adet,fiyat,ops1,ops2,ops3);
        //}

        if (e.CommandName == "Detay")
        {
            int satirIndeksi = Convert.ToInt32(e.CommandArgument);
            GridViewRow satir = GridView1.Rows[satirIndeksi];
            //string yemekID = ((Label)satir.FindControl("Label21")).Text;
            string yemekID = satir.Cells[3].Text;
            Response.Redirect("YemekDetay.aspx?YemekID=" + yemekID);
        }
    }


    //void SepeteEkle(string restID,string yemekID,string adet,string fiyat,string ops1,string ops2, string ops3)
    //{

    //    SqlConnection baglanti = new SqlConnection(baglantiYolu);
    //    SqlCommand komut = new SqlCommand();
    //    komut.Connection = baglanti;
    //    komut.CommandText = "insert into Sepet(RestoranID,MusteriID,YemekID,Ops1,Ops2,Ops3,Fiyat,Adet) values (@prestID,@pmusteriID,@pyemekID,@pops1,@pops2,@pops3,@pfiyat,@padet)";
    //    komut.Parameters.Clear();
    //    komut.Parameters.AddWithValue(@"prestID", restID);
    //    komut.Parameters.AddWithValue(@"pmusteriID",Session["MusteriID"]);
    //    komut.Parameters.AddWithValue(@"pyemekID",yemekID);
    //    komut.Parameters.AddWithValue(@"pops1", ops1);
    //    komut.Parameters.AddWithValue(@"pops2", ops2);
    //    komut.Parameters.AddWithValue(@"pops3", ops3);
    //    komut.Parameters.AddWithValue(@"pfiyat",fiyat);
    //    komut.Parameters.AddWithValue(@"padet", adet);


    //    try
    //    {
    //        baglanti.Open();
    //        komut.ExecuteNonQuery();
    //        Response.Write("Yemek sepete eklendi.");
    //    }
    //    catch(Exception ex) { Response.Write(ex.Message); }
    //    finally { baglanti.Close(); }

    //}

}