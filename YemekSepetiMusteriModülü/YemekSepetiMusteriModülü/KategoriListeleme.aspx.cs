﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class KategoriListeleme : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                DataSet kategoriler = KategoriCek();
                GridView1.DataSource = kategoriler.Tables[0];
                GridView1.DataBind();
            }

        }
        else
        Response.Redirect("Giris.aspx");
    }

    DataSet KategoriCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select KategoriID as ID,KategoriAdi from Kategori order by KategoriAdi";
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet kategoriler = new DataSet();
        adaptor.Fill(kategoriler);

        return kategoriler;
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet kategoriler = KategoriCek();
        GridView1.DataSource = kategoriler.Tables[0];
        GridView1.DataBind();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Sec")
        {
            int satirIndeksi = Convert.ToInt32(e.CommandArgument);
            GridViewRow satir = GridView1.Rows[satirIndeksi];
            string KategoriID = satir.Cells[1].Text;
            Response.Redirect("KategoriYemekListele.aspx?KategoriID=" + KategoriID);
        }
    }
}