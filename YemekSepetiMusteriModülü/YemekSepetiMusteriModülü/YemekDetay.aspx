﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YemekDetay.aspx.cs" Inherits="YemekDetay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Yemek Detayları</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                <%--<asp:Image ID="Image1" Height = "240" Width = "300" DataField="Resmi" runat="server" ImageUrl='<%#"data:Image/png;base64,"+ Convert.ToBase64String((byte[])Eval("Resmi"))%>'></asp:Image>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("YemekID") %>' Width="70px" Visible="false"></asp:Label>
        <asp:Label ID="Label7" runat="server" Visible="false" Text='<%# Eval("RestoranID") %>'></asp:Label>


                <br />
                Yemek Adı:&nbsp<asp:Label ID="Label2" runat="server" Width="100px" Text='<%# Eval("YemekAdi") %>'></asp:Label>
                <br />
        Hazırlanma Süresi:&nbsp<asp:Label ID="Label3" runat="server" Width="100px" Text='<%# Eval("HazirlanmaSuresi") %>'></asp:Label>
        <br />
        Açıklama:&nbsp<asp:Label ID="Label4" runat="server" Width="250px" Text='<%# Eval("Aciklama") %>'></asp:Label>

                <br />
                Fiyat:&nbsp<asp:Label ID="Label5" runat="server" Width="40px" Text='<%# Eval("Fiyat") %>'></asp:Label>
        <br />
        Bulunduğu Restoran:&nbsp<asp:Label ID="Label6" runat="server" Width="100px" Text='<%# Eval("RestoranAdi") %>'></asp:Label>
                <br />--%>
                <asp:Label ID="LBL" Text="Yemeğin Detayları" runat="server" Font-Bold="true" ></asp:Label>
                <br />
       <asp:GridView ID="GV1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" style="margin-right: 0px" AutoGenerateColumns="False">
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" BorderStyle="Solid" HorizontalAlign="Center" VerticalAlign="Middle" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" HorizontalAlign="Left"/>
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" /><Columns>
            <asp:TemplateField HeaderText="Resmi">
                <ItemTemplate><asp:Image ID="Image1" Height = "240" Width = "300" DataField="Resmi" runat="server" ImageUrl='<%#"data:Image/png;base64,"+ Convert.ToBase64String((byte[])Eval("Resmi"))%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Adi" HeaderText="Restoran Adı" />
            <asp:BoundField DataField="RestoranID" HeaderText="Restoran ID" />
            <asp:BoundField DataField="YemekID" HeaderText="Yemek ID" />
                <asp:BoundField DataField="YemekAdi" HeaderText="Yemek Adı" />
                <asp:BoundField DataField="HazirlanmaSuresi" HeaderText="Hazırlanma Süresi(Dk)" />
                <asp:BoundField DataField="Aciklama" HeaderText="Açıklama" />
                <asp:BoundField DataField="Fiyat" HeaderText="Fiyat(₺)" />
            
                      </Columns></asp:GridView>
                <br />
                &nbsp Adet:&nbsp<asp:TextBox ID="txtAdet" Text="1" runat="server" Width="72px"></asp:TextBox>
                
                &nbsp&nbsp&nbsp Opsiyonlar:&nbsp<asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
        &nbsp<asp:DropDownList ID="DropDownList2" runat="server">
                </asp:DropDownList>
        &nbsp<asp:DropDownList ID="DropDownList3" runat="server">
                </asp:DropDownList>
                &nbsp&nbsp&nbsp&nbsp<asp:Button ID="Button1" runat="server" Height="36px" Text="Sepete Ekle" CommandName="SepeteEkle" Width="191px" OnClick="Button1_Click" />
                <br />
        <br />

        <a href="Sepet.aspx">Sepete Git</a><br />
        <a href="YemekListele.aspx">Geri Dön</a>


<br />
    </div>
        
    </form>
</body>
</html>
