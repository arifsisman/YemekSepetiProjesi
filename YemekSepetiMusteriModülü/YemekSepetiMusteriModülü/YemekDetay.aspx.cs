﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class YemekDetay : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                if (Request.QueryString["YemekID"] != null) {
                    DataSet yemek = YemekCek();
                    GV1.DataSource = yemek.Tables[0];
                    GV1.DataBind();

                    DropDownList1.DataTextField = "OpsiyonAdi";
                    DropDownList1.DataValueField = "OpsiyonID";

                    DropDownList2.DataTextField = "OpsiyonAdi";
                    DropDownList2.DataValueField = "OpsiyonID";

                    DropDownList3.DataTextField = "OpsiyonAdi";
                    DropDownList3.DataValueField = "OpsiyonID";

                    DropDownList1.DataSource = OpsiyonCek().Tables[0];
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, new ListItem("Seçilmedi", string.Empty));
                    DropDownList2.DataSource = OpsiyonCek().Tables[0];
                    DropDownList2.DataBind();
                    DropDownList2.Items.Insert(0, new ListItem("Seçilmedi", string.Empty));
                    DropDownList3.DataSource = OpsiyonCek().Tables[0];
                    DropDownList3.DataBind();
                    DropDownList3.Items.Insert(0, new ListItem("Seçilmedi", string.Empty));
                }          

            }

        }
        else
            Response.Redirect("Giris.aspx");
    }

    DataSet YemekCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select * from Yemek,Restoran where Yemek.RestoranID=Restoran.RestoranID and YemekID="+Request.QueryString["YemekID"];

        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet donenVeri = new DataSet();
        adaptor.Fill(donenVeri);
        return donenVeri;
    }

    DataSet OpsiyonCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select * from Opsiyon,YemekOpsiyonu where Opsiyon.OpsiyonID=YemekOpsiyonu.OpsiyonID and YemekID=" + Request.QueryString["YemekID"];
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet donenVeri = new DataSet();
        adaptor.Fill(donenVeri);
        return donenVeri;
    }


    protected void Button1_Click(object sender, EventArgs e)
    {

        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "insert into Sepet(RestoranID,MusteriID,YemekID,Ops1,Ops2,Ops3,Fiyat,Adet) values (@prestID,@pmusteriID,@pyemekID,@pops1,@pops2,@pops3,@pfiyat,@padet)";
        komut.Parameters.Clear();
        komut.Parameters.AddWithValue(@"prestID", GV1.Rows[0].Cells[2].Text);
        komut.Parameters.AddWithValue(@"pmusteriID", Session["MusteriID"]);
        komut.Parameters.AddWithValue(@"pyemekID", Request.QueryString["YemekID"]);
        komut.Parameters.AddWithValue(@"pops1", DropDownList1.SelectedItem.Text);
        komut.Parameters.AddWithValue(@"pops2", DropDownList2.SelectedItem.Text);
        komut.Parameters.AddWithValue(@"pops3", DropDownList3.SelectedItem.Text);
        komut.Parameters.AddWithValue(@"pfiyat", GV1.Rows[0].Cells[7].Text);
        komut.Parameters.AddWithValue(@"padet", txtAdet.Text);

        try
        {
            baglanti.Open();
            komut.ExecuteNonQuery();
            Response.Write("Yemek sepete eklendi.");
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        finally { baglanti.Close(); }
    }
}