﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class SepetOnay : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();
    bool adresVarMi = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                Label6.Text="Sepetin toplam tutarı= " + Session["ToplamFiyat"] + " Türk Lirası";
                SqlConnection baglanti = new SqlConnection(baglantiYolu);
                SqlCommand komut = new SqlCommand();
                komut.Connection = baglanti;
                komut.CommandText = "select Adres from Musteri where MusteriID="+Session["MusteriID"];
                SqlDataAdapter adaptor = new SqlDataAdapter();
                DataSet adres = new DataSet();
                adaptor.SelectCommand = komut;
                adaptor.Fill(adres);

                TextBox1.Text = adres.Tables[0].Rows[0]["Adres"].ToString();
                if (TextBox1.Text==null)
                    adresVarMi= false;
            }

        }
        else
            Response.Redirect("Giris.aspx");
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text == "")
            Response.Write("Adres giriniz.");
        else if (TextBox2.Text == "" || TextBox3.Text == "" || TextBox4.Text == "")
            Response.Write("Kart bilgilerini eksiksiz giriniz.");
        else
        { 
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "insert into Siparis(RestoranID,MusteriID,YemekID,Ops1,Ops2,Ops3,Fiyat,Adet) select RestoranID,MusteriID,YemekID,Ops1,Ops2,Ops3,Fiyat,Adet from Sepet where MusteriID=" + Session["MusteriID"];

        try
        {
            baglanti.Open();
            komut.ExecuteNonQuery();
            Response.Write("Siparişiniz alındı.\t");
            SepettenCikar();
            if (adresVarMi == false)
                AdresGir();
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        finally { baglanti.Close(); }
        }
    }

    void SepettenCikar()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "delete from Sepet where MusteriID="+Session["MusteriID"];

        baglanti.Open();
        komut.ExecuteNonQuery();
        baglanti.Close();
    }

    void AdresGir()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "insert into Musteri Adres=@padres where MusteriID="+Session["MusteriID"];
        komut.Parameters.Clear();
        komut.Parameters.AddWithValue(@"padres",TextBox1.Text);

        try
        {
            baglanti.Open();
            komut.ExecuteNonQuery();
            Response.Write("Adres bilgileriniz güncellendi\t");
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        finally { baglanti.Close(); }

    }


}