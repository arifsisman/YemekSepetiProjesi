﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class KayitOl : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["MGirisYetkisi"] = false;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox4.Text == TextBox6.Text && TextBox4.Text !="" && TextBox6.Text != "")
        {
            SqlConnection baglanti = new SqlConnection(baglantiYolu);
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandText = "insert into Musteri(Ad,Soyad,Mail,Sifre,Adres) values(@pad,@psoyad,@pmail,@psifre,@padres)";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"pad", TextBox1.Text);
            komut.Parameters.AddWithValue(@"psoyad", TextBox2.Text);
            komut.Parameters.AddWithValue(@"pmail", TextBox3.Text);
            komut.Parameters.AddWithValue(@"psifre", TextBox4.Text);
            komut.Parameters.AddWithValue(@"padres", TextBox5.Text);

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                Response.Write("Kaydınız başarıyla tamamlandı.");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }
        }
        else
            Response.Write("Girdiğiniz şifreler uyuşmuyor.Lütfen tekrar deneyiniz.");
    }
}