﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Siparis.aspx.cs" Inherits="SepetOnay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sipariş</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="Adres:" Font-Bold="True"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox1" runat="server" Height="115px" Width="389px" TextMode="MultiLine"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Ödeme Bilgileri:" Font-Bold="True"></asp:Label>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Kartın Sahibi:"></asp:Label>&nbsp&nbsp&nbsp&nbsp
        <asp:TextBox ID="TextBox2" runat="server" Width="180px"></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Kart Numarası:"></asp:Label>&nbsp
        <asp:TextBox ID="TextBox3" runat="server" Width="180px" MaxLength="16"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="CVV3:"></asp:Label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <asp:TextBox ID="TextBox4" runat="server" Width="180px" MaxLength="3"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label6" runat="server" Text="&quot;ToplamTutar&quot;" Font-Bold="True"></asp:Label>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Height="36px" OnClick="Button1_Click" Text="Sipariş Ver" Width="175px" />
        <br />
        <br />
        <br />
        <a href="MusteriIslemler.aspx">Geri Dön</a>
        </div>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
