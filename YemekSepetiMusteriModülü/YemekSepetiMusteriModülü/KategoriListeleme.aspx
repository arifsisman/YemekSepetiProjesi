﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KategoriListeleme.aspx.cs" Inherits="KategoriListeleme" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kategori Listeleme</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>   
        <asp:Label ID="LBL" Text="Kategori Listeleme" runat="server" Font-Bold="true" ></asp:Label>
                <br />
                <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" OnRowCommand="GridView1_RowCommand" >
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    <Columns>
                                                        <asp:TemplateField HeaderText="Yemekler" >
                                    <ItemTemplate>
                                        <asp:Button ID="Sec" Text="Yemekleri Gör" runat="server" CommandName="Sec" CommandArgument="<%# Container.DataItemIndex %>"/>
                                    </ItemTemplate>
            </asp:TemplateField>
                    </Columns>
        </asp:GridView>
        <br />

        <a href="MusteriIslemler.aspx">Geri Dön</a>

        <br />
    </div>
    </form>
</body>
</html>
