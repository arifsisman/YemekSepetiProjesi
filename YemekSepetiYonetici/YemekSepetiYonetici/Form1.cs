﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace YemekSepetiYonetici
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string baglantiYolu = @"Data Source=MUSTAFA\SQLEXPRESS;Initial Catalog=YemekSepeti;Integrated Security=True";
        private void button1_Click(object sender, EventArgs e)
        {
            string yoneticiAdi = "";
            SqlConnection baglanti = new SqlConnection(baglantiYolu);
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandText = "select Ad,Soyad from Yonetici where YoneticiID=@pno and Sifre=@psifre";
            komut.Parameters.AddWithValue(@"pno", textBox1.Text);
            komut.Parameters.AddWithValue(@"psifre", textBox2.Text);
            DataSet donenVeri = new DataSet();
            SqlDataAdapter adaptor = new SqlDataAdapter();
            adaptor.SelectCommand = komut;
            adaptor.Fill(donenVeri);

            if (donenVeri.Tables[0].Rows.Count > 0)
            {
                this.Hide();
                Form2 frm2 = new Form2(); 
                string ad="", soyad="",ID=textBox1.Text;
                ad=donenVeri.Tables[0].Rows[0]["Ad"].ToString();
                soyad= donenVeri.Tables[0].Rows[0]["Soyad"].ToString();
                yoneticiAdi = ad +" "+ soyad+" (ID="+ID+")";
                frm2.lbl.Text = yoneticiAdi;
                frm2.Show();
            }
            else
                MessageBox.Show("Yanlış ID veya şifre.");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
