﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class RestoranListeleme : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                DataSet restoranlar = RestoranCek();
                GridView1.DataSource = restoranlar.Tables[0];
                GridView1.DataBind();
            }

        }
        else
            Response.Redirect("Giris.aspx");
    }

    DataSet RestoranCek()
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select RestoranID,Adi,Telefon,Resmi,Adres from Restoran";
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet restoran = new DataSet();
        adaptor.Fill(restoran);

        return restoran;

    }



    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Sec")
        {
            int satirIndeksi = Convert.ToInt32(e.CommandArgument);
            GridViewRow satir = GridView1.Rows[satirIndeksi];
            string restoranID = satir.Cells[2].Text;
            Response.Redirect("RestoranYemekListele.aspx?RestoranID=" + restoranID);
        }
    }
}