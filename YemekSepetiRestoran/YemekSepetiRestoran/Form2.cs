﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace YemekSepetiRestoran
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public string restAd;
        public string restID;

        string baglantiYolu = @"Data Source=MUSTAFA\SQLEXPRESS;Initial Catalog=YemekSepeti;Integrated Security=True";
        SqlConnection baglanti;
        SqlDataAdapter adaptor;
        SqlCommand komut;
        string resimYolu = "";
        bool secilenResimVar = false;

        private void Form2_Load(object sender, EventArgs e)
        {
            label3.Text = "Siparişleri görüntülenen restoran: " + restAd + "  (RestoranID=" + restID + ")";
            baglanti = new SqlConnection(baglantiYolu);
            komut = new SqlCommand();
            komut.Connection = baglanti;
            adaptor = new SqlDataAdapter();

            comboBox1.ValueMember = "KategoriID";
            comboBox1.DisplayMember = "KategoriAdi";
            DataSet donenVeriler1 = KategoriAdiCek();
            comboBox1.DataSource = donenVeriler1.Tables[0];

            comboBox2.ValueMember = "KategoriID";
            comboBox2.DisplayMember = "KategoriAdi";
            DataSet donenVeriler2 = KategoriAdiCek();
            comboBox2.DataSource = donenVeriler2.Tables[0];

            comboBox3.ValueMember = "YemekID";
            comboBox3.DisplayMember = "YemekAdi";
            DataSet donenVeriler3 = YemekAdiCek();
            comboBox3.DataSource = donenVeriler3.Tables[0];

            comboBox4.ValueMember = "KategoriID";
            comboBox4.DisplayMember = "KategoriAdi";
            DataSet donenVeriler4 = KategoriAdiCek();
            comboBox4.DataSource = donenVeriler4.Tables[0];

            comboBox5.ValueMember = "KategoriID";
            comboBox5.DisplayMember = "KategoriAdi";
            DataSet donenVeriler5 = KategoriCek();
            comboBox5.DataSource = donenVeriler5.Tables[0];//

            comboBox6.ValueMember = "OpsiyonID";
            comboBox6.DisplayMember = "OpsiyonAdi";
            DataSet donenVeriler6 = OpsiyonCek();
            comboBox6.DataSource = donenVeriler6.Tables[0];

            comboBox7.ValueMember = "OpsiyonID";
            comboBox7.DisplayMember = "OpsiyonAdi";
            DataSet donenVeriler7 = OpsiyonCek();
            comboBox7.DataSource = donenVeriler7.Tables[0];

            comboBox8.ValueMember = "OpsiyonID";
            comboBox8.DisplayMember = "OpsiyonAdi";
            DataSet donenVeriler8 = OpsiyonCek();
            comboBox8.DataSource = donenVeriler8.Tables[0];


            if (comboBox3.SelectedItem != null)//Seçilen restorana ait yemek bulunuyorsa ilk yemeği düzenleme tablosuna aktarıyor.
            {
                YemekKategoriCek();
                YemekOpsiyonCek();
            }
                //ilk önce kategoriler çekiliyor daha sonra hangi kategoriye ait ise o seçiliyor ve diğer bilgiler çekiliyor.
            SiparisCek();

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        void SiparisCek()
        {
            komut.CommandText = "Select SiparisID,Adet,Yemek.YemekAdi,Siparis.YemekID,Kategori.KategoriAdi,Musteri.Ad,Musteri.Soyad,Musteri.Adres,Ops1 as Opsiyon1,Ops2 as Opsiyon2,Ops3 as Opsiyon3,Siparis.Fiyat,Resmi from Siparis,Yemek,Kategori,Musteri where Musteri.MusteriID=Siparis.MusteriID and Yemek.YemekID=Siparis.YemekID and Yemek.KategoriID=Kategori.KategoriID and Siparis.RestoranID=" + restID;
            DataSet siparisler = new DataSet();
            adaptor.SelectCommand = komut;
            adaptor.Fill(siparisler);

            dataGridView1.DataSource = siparisler.Tables[0];
        }

        void KategoriEkle()
        {
            komut.CommandText = "insert into RestoranKategori(RestoranID,KategoriID,KategoriAdi) values (@prestoranID,@pkategoriID,@pkategoriAdi)";//RESTORANKATEGORİ OLARAK DEĞİŞTİR
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"prestoranID", restID);
            komut.Parameters.AddWithValue(@"pkategoriID", comboBox5.SelectedValue);
            komut.Parameters.AddWithValue(@"pkategoriAdi", comboBox5.Text);

            baglanti.Open();
            komut.ExecuteNonQuery();
            baglanti.Close();
            MessageBox.Show("Kategori restorana eklendi.");
        }

        DataSet KategoriCek()
        {
            //tüm kategorileri çeker
            komut.CommandText = "select * from Kategori ";

            adaptor.SelectCommand = komut;
            DataSet kategoriler = new DataSet();
            adaptor.Fill(kategoriler);

            return kategoriler;
        }

        DataSet KategoriAdiCek()//restoranda bulunan kategorileri çekmeye yarar
        {
            komut.CommandText = "select * from RestoranKategori where RestoranID=" + restID;

            adaptor.SelectCommand = komut;
            DataSet donenVeri = new DataSet();
            adaptor.Fill(donenVeri);

            return donenVeri;
        }

        DataSet YemekAdiCek()//restoranda bulunan yemekleri çekme
        {
            komut.CommandText = "select * from Yemek where RestoranID=" + restID;
            adaptor.SelectCommand = komut;
            DataSet yemekler = new DataSet();
            adaptor.Fill(yemekler);


            return yemekler;
        }

        void YemekKategoriCek()//yemek ve kategori tablosunu join ile çekme
        {////////////////////////7

            komut.CommandText = "select * from Yemek,Kategori where Yemek.KategoriID=Kategori.KategoriID and YemekID=@pyemekID"; //and'den sonrasını çıkarıp dene
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"pyemekID", comboBox3.SelectedValue);

            adaptor.SelectCommand = komut;
            DataSet yemekKategori = new DataSet();
            adaptor.Fill(yemekKategori);

            textBox17.Text = yemekKategori.Tables[0].Rows[0]["YemekID"].ToString();
            textBox16.Text = yemekKategori.Tables[0].Rows[0]["YemekAdi"].ToString();
            //
            comboBox4.Text = yemekKategori.Tables[0].Rows[0]["KategoriAdi"].ToString();
            comboBox4.SelectedValue = yemekKategori.Tables[0].Rows[0]["KategoriID"].ToString();
            //
            MemoryStream ms = new MemoryStream((byte[])yemekKategori.Tables[0].Rows[0]["Resmi"]);
            pictureBox2.Image = Image.FromStream(ms);//streamden image okuma
            textBox15.Text = yemekKategori.Tables[0].Rows[0]["HazirlanmaSuresi"].ToString();
            textBox14.Text = yemekKategori.Tables[0].Rows[0]["Aciklama"].ToString();
            textBox13.Text = yemekKategori.Tables[0].Rows[0]["Fiyat"].ToString();
            YemekOpsiyonCek();


        }

        void YemekOpsiyonCek()
        {
            comboBox6.Text = "";
            comboBox6.SelectedValue = -1;
            comboBox7.Text = "";
            comboBox7.SelectedValue = -1;
            comboBox8.Text = "";
            comboBox8.SelectedValue = -1;
            komut.CommandText = "select * from Opsiyon,YemekOpsiyonu where Opsiyon.OpsiyonID=YemekOpsiyonu.OpsiyonID and YemekID="+textBox17.Text;
            komut.Parameters.Clear();
            DataSet opsiyon = new DataSet();
            adaptor.Fill(opsiyon);
            try
            {
                comboBox6.SelectedItem = opsiyon.Tables[0].Rows[0]["OpsiyonAdi"].ToString();
                comboBox6.SelectedValue = opsiyon.Tables[0].Rows[0]["OpsiyonID"].ToString();
                comboBox7.SelectedItem = opsiyon.Tables[0].Rows[1]["OpsiyonAdi"].ToString();
                comboBox7.SelectedValue = opsiyon.Tables[0].Rows[1]["OpsiyonID"].ToString();
                comboBox8.SelectedItem = opsiyon.Tables[0].Rows[2]["OpsiyonAdi"].ToString();
                comboBox8.SelectedValue = opsiyon.Tables[0].Rows[2]["OpsiyonID"].ToString();
            }
            catch { }
        }


        void KategoriSil()
        {
            komut.CommandText = "delete from RestoranKategori where RestoranID=@prestID and KategoriID=@pkategoriID";//RESTORANKATEGORİ OLARAK DEĞİŞTİR

            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"prestID", restID);
            komut.Parameters.AddWithValue(@"pkategoriID", comboBox1.SelectedValue);
            baglanti.Open();
            komut.ExecuteNonQuery();
            baglanti.Close();
            MessageBox.Show("Seçili kategori restorandan silindi.");
            KategoriCek();
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";

            DataSet donenVeriler = KategoriAdiCek();
            comboBox1.DataSource = donenVeriler.Tables[0];//combobox1'in listesini yeniliyoruz
        }

        void YemekEkle(string adi, string kategoriID, string restoranID, string hSuresi, string aciklama, string fiyat)
        {
            FileStream fs = new FileStream(resimYolu, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] resim = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();

            komut.CommandText = "insert into Yemek values (@pkategoriID,@prestoranID,@padi,@presim,@phSuresi,@paciklama,@pfiyat)";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue("@pkategoriID", kategoriID);
            komut.Parameters.AddWithValue("@prestoranID", restoranID);
            komut.Parameters.AddWithValue("@padi", adi);
            komut.Parameters.Add("@presim", SqlDbType.Image, resim.Length).Value = resim;
            komut.Parameters.AddWithValue("@phSuresi", hSuresi);
            komut.Parameters.AddWithValue("@paciklama", aciklama);
            komut.Parameters.AddWithValue("@pfiyat", fiyat);

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Yemek eklendi.");
                resimYolu = "";
                button17.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
                
            }
        }

        void YemekGuncelle()
        {
            if (secilenResimVar == true)
            {
                FileStream fs = new FileStream(resimYolu, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] resim = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();

                komut.CommandText = "Update Yemek set KategoriID=@pkategoriID,YemekAdi=@padi,Resmi=@presim,HazirlanmaSuresi=@psuresi,Aciklama=@paciklama,Fiyat=@pfiyat where YemekID=" + textBox17.Text;

                komut.Parameters.Clear();
                komut.Parameters.AddWithValue(@"pkategoriID", comboBox4.SelectedValue);
                komut.Parameters.Add("@presim", SqlDbType.Image, resim.Length).Value = resim;
                komut.Parameters.AddWithValue(@"padi", textBox16.Text);
                komut.Parameters.AddWithValue(@"psuresi", textBox15.Text);
                komut.Parameters.AddWithValue(@"paciklama", textBox14.Text);
                komut.Parameters.AddWithValue(@"pfiyat", textBox13.Text);
            }
            else
            {
                komut.CommandText = "Update Yemek set KategoriID=@pkategoriID,YemekAdi=@padi,HazirlanmaSuresi=@psuresi,Aciklama=@paciklama,Fiyat=@pfiyat where YemekID=" + textBox17.Text;

                komut.Parameters.Clear();
                komut.Parameters.AddWithValue(@"pkategoriID", comboBox4.SelectedValue);
                komut.Parameters.AddWithValue(@"padi", textBox16.Text);
                komut.Parameters.AddWithValue(@"psuresi", textBox15.Text);
                komut.Parameters.AddWithValue(@"paciklama", textBox14.Text);
                komut.Parameters.AddWithValue(@"pfiyat", textBox13.Text);
            }

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Yemek bilgileri güncellendi.");
                resimYolu = "";
                secilenResimVar = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }

        }

        void YemekSil()
        {
            komut.CommandText = "delete from Yemek where YemekID=" + textBox17.Text;

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Yemek silindi.");
                YemekKategoriCek();
            }
            catch
            {
                DataSet donenVeriler2 = YemekAdiCek();
                comboBox3.DataSource = donenVeriler2.Tables[0];
            }
            finally
            {
                baglanti.Close();
            }

        }


        void OpsiyonEkle(string opsiyonAdi)
        {
            komut.CommandText = "insert into YemekOpsiyonu(YemekID,OpsiyonID) values (@pyemek,@popsiyonID)";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"pyemek", textBox17.Text);
            komut.Parameters.AddWithValue(@"popsiyonID", opsiyonAdi);

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Opsiyon eklendi.");
                OpsiyonCek();
            }
            catch
            {
                MessageBox.Show("Lütfen ilk önce mevcut opsiyonu siliniz");
            }
            finally
            {
                baglanti.Close();
            }

        }

        DataSet OpsiyonCek()
        {
            komut.CommandText = "select OpsiyonAdi,OpsiyonID from Opsiyon";
            DataSet opsiyon = new DataSet();
            adaptor.Fill(opsiyon);
            return opsiyon;
        }

        void OpsiyonSil(string opsiyonID)
        {
            komut.CommandText = "delete from YemekOpsiyonu where OpsiyonID=@pID and YemekID=" + textBox17.Text;
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"pID", opsiyonID);
            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Opsiyon silindi.");
                YemekOpsiyonCek();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            KategoriEkle();
            //alttaki combobox'ı yenilemek için
            DataSet donenVeriler = KategoriAdiCek();
            comboBox1.DataSource = donenVeriler.Tables[0];
        }



        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1 frm1 = new Form1();
            frm1.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files|*.jpg; *.png; *.bmp";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(ofd.FileName);
                resimYolu = ofd.FileName.ToString();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files|*.jpg; *.png; *.bmp";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = Image.FromFile(ofd.FileName);
                resimYolu = ofd.FileName.ToString();
            }
            secilenResimVar = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (resimYolu != "")
            {
                string kategori = comboBox2.SelectedValue.ToString();
                string adi = textBox5.Text;
                string suresi = textBox6.Text;
                string aciklama = textBox7.Text;
                string fiyat = textBox8.Text;
                string restoranID = restID;

                YemekEkle(adi, kategori, restoranID, suresi, aciklama, fiyat);
            }
            else
                MessageBox.Show("Lütfen yemek resmini seçiniz.");
        }


        private void button4_Click(object sender, EventArgs e)
        {
            KategoriSil();
        }


        public void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            YemekKategoriCek();
            YemekOpsiyonCek();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            YemekGuncelle();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            YemekSil();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            komut.CommandText = "select KategoriID,KategoriAdi from RestoranKategori where KategoriID=" + comboBox1.SelectedValue;

            adaptor.SelectCommand = komut;
            DataSet donenVeriler = new DataSet();
            adaptor.Fill(donenVeriler);

            textBox1.Text = restID;
            textBox2.Text = donenVeriler.Tables[0].Rows[0]["KategoriID"].ToString();
            textBox3.Text = donenVeriler.Tables[0].Rows[0]["KategoriAdi"].ToString();

        }

        private void comboBox3_Click(object sender, EventArgs e)
        {
            DataSet donenVeriler2 = YemekAdiCek();
            comboBox3.DataSource = donenVeriler2.Tables[0];
        }


        private void button14_Click_1(object sender, EventArgs e)
        {
            OpsiyonSil(comboBox6.SelectedValue.ToString());
        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            OpsiyonSil(comboBox7.SelectedValue.ToString());
        }

        private void button16_Click_1(object sender, EventArgs e)
        {
            OpsiyonSil(comboBox8.SelectedValue.ToString());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OpsiyonEkle(comboBox6.SelectedValue.ToString());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpsiyonEkle(comboBox7.SelectedValue.ToString());
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            OpsiyonEkle(comboBox8.SelectedValue.ToString());
        }

        private void button17_Click(object sender, EventArgs e)
        {
            tabControl3.SelectedTab = tabPage7;
            DataSet donenVeriler2 = YemekAdiCek();
            comboBox3.DataSource = donenVeriler2.Tables[0];
            comboBox3.SelectedIndex = comboBox3.Items.Count - 1;
            button17.Visible = false;
        }

        private void comboBox2_Click(object sender, EventArgs e)
        {
            DataSet donenVeriler = KategoriAdiCek();
            comboBox2.DataSource = donenVeriler.Tables[0];
        }

        private void comboBox4_Click(object sender, EventArgs e)
        {
            DataSet kategoriler = KategoriAdiCek();
            comboBox4.DataSource = kategoriler.Tables[0];
        }
    }
}
