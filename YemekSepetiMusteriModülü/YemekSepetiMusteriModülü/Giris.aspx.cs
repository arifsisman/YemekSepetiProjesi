﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class GirisSayfasi : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["MGirisYetkisi"] = false;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "Select MusteriID,Ad,Soyad,Adres from Musteri where Mail=@pmail and Sifre=@psifre";
        komut.Parameters.AddWithValue(@"pmail",TextBox1.Text);
        komut.Parameters.AddWithValue(@"psifre", TextBox2.Text);
        DataSet donenVeriler = new DataSet();
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        adaptor.Fill(donenVeriler);

        if (donenVeriler.Tables[0].Rows.Count > 0)
        {
            Session["MGirisYetkisi"] = true;
            Session["MAd"] = donenVeriler.Tables[0].Rows[0]["Ad"].ToString();
            Session["MSoyad"] = donenVeriler.Tables[0].Rows[0]["Soyad"].ToString();
            Session["MusteriID"] = donenVeriler.Tables[0].Rows[0]["MusteriID"].ToString();
            Session["MMail"] = TextBox1.Text;
            Response.Redirect("MusteriIslemler.aspx");
        }
        else
            Response.Write("Yanlış mail veya şifre");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("KayitOl.aspx");
    }
}