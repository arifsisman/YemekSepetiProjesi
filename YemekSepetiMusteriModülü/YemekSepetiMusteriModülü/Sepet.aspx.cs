﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Sepet : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                SepeteYemekCek();
                try
                {
                    if (GridView1.Rows[0].Cells[4].Text != null)
                        Button3.Visible = true;
                }
                catch { }
            }
        }
        else
            Response.Redirect("Giris.aspx");
    }

    void SepeteYemekCek()
    {
        DataSet sepet = SepetCek();
        GridView1.DataSource = sepet.Tables[0];
        GridView1.DataBind();
        //OpsiyonTick();
        ToplamFiyatHesapla();
    }
    //void OpsiyonTick()
    //{
    //    foreach (GridViewRow row in GridView1.Rows)
    //    {
    //        if(((Label)row.FindControl("Label17")).Text!="")
    //            ((CheckBox)row.FindControl("CheckBox1")).Checked = true;
    //        if (((Label)row.FindControl("Label18")).Text != "")
    //            ((CheckBox)row.FindControl("CheckBox2")).Checked = true;
    //        if (((Label)row.FindControl("Label19")).Text != "")
    //            ((CheckBox)row.FindControl("CheckBox3")).Checked = true;
    //    }
    //}

    DataSet SepetCek()
    {               
        SqlConnection baglanti = new SqlConnection(baglantiYolu);
        SqlCommand komut = new SqlCommand();
        komut.Connection = baglanti;
        komut.CommandText = "select Sepet.SepetID,Sepet.YemekID,YemekAdi,Yemek.RestoranID,Restoran.RestoranID,Restoran.Adi,Yemek.Resmi,HazirlanmaSuresi,Aciklama,Yemek.Fiyat,Ops1 as Opsiyon1,Ops2 as Opsiyon2,Ops3 as Opsiyon3,Adet,MusteriID from Yemek,Sepet,Restoran where Yemek.RestoranID=Restoran.RestoranID and Yemek.YemekID=Sepet.YemekID and MusteriID="+Session["MusteriID"];
        SqlDataAdapter adaptor = new SqlDataAdapter();
        adaptor.SelectCommand = komut;
        DataSet sepet = new DataSet();
        adaptor.Fill(sepet);

        return sepet;
    }

    void ToplamFiyatHesapla()
    {
        double toplamFiyat=0,fiyat=0;
        int adet;
        foreach (GridViewRow row in GridView1.Rows)
        {
            adet = Convert.ToInt32(((TextBox)row.Cells[0].FindControl("txtAdet")).Text);
            fiyat = Convert.ToDouble(row.Cells[12].Text);
            toplamFiyat += adet*fiyat;
        }
        Session["ToplamFiyat"] = toplamFiyat;
        //2.yol
        //int i = 0;
        //do
        //{
        //adet = Convert.ToInt32(((TextBox)row.FindControl("txtAdet")).Text);
        //fiyat = Convert.ToDouble(((Label)row.FindControl("Label16")).Text);
        //toplamFiyat += adet * fiyat;
        //    i++;
        //} while (i < GridView1.Rows.Count);

        //3.yol
        //for (int i = 0; i < GridView1.Rows.Count; i++)
        //{
        //    GridViewRow row = GridView1.Rows[i];
        //adet = Convert.ToInt32(((TextBox)row.FindControl("txtAdet")).Text);
        //fiyat = Convert.ToDouble(((Label)row.FindControl("Label16")).Text);
        //toplamFiyat += adet * fiyat;
        //}

        Label23.Text = "Sepetinizin toplam tutarı= " + toplamFiyat + " Türk Lirası";
        Label23.DataBind();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Guncelle")
        {
            int satirIndeksi = Convert.ToInt32(e.CommandArgument);
            GridViewRow satir = GridView1.Rows[satirIndeksi];
            SqlConnection baglanti = new SqlConnection(baglantiYolu);
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandText = "update Sepet set Adet=@padet where SepetID=@psepetID";
            komut.Parameters.Clear();

            komut.Parameters.AddWithValue("@padet", (((TextBox)satir.Cells[0].FindControl("txtAdet")).Text));
            komut.Parameters.AddWithValue(@"psepetID", satir.Cells[13].Text);
            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                Response.Write("Sepet güncellendi.");
                ToplamFiyatHesapla();
            }
            catch (Exception ex) { Response.Write(ex.Message); }
            finally { baglanti.Close(); }
        }

        else if (e.CommandName == "Sil")
        {
            int satirIndeksi = Convert.ToInt32(e.CommandArgument);
            GridViewRow satir = GridView1.Rows[satirIndeksi];
            SqlConnection baglanti = new SqlConnection(baglantiYolu);
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandText="delete from Sepet where SepetID=@psepetID";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue(@"psepetID", satir.Cells[13].Text);

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                Response.Write("Seçilen yemek sepetten çıkarıldı.");
                SepeteYemekCek();
            }
            catch (Exception ex) { Response.Write(ex.Message); }
            finally { baglanti.Close(); }
        }

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("Siparis.aspx");
    }
}