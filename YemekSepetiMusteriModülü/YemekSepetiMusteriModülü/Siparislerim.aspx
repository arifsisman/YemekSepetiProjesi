﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Siparislerim.aspx.cs" Inherits="Siparislerim" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Siparişlerim</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LBL" Text="Siparişlerim" runat="server" Font-Bold="true" ></asp:Label>
                <br />
        <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" style="margin-right: 0px" AutoGenerateColumns="False">
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" BorderStyle="Solid" HorizontalAlign="Center" VerticalAlign="Middle" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" HorizontalAlign="Left"/>
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
            <Columns>

                <asp:BoundField DataField="Adet" HeaderText="Adet" />
                                <asp:TemplateField HeaderText="Resmi">
                <ItemTemplate><asp:Image ID="Image1" Height = "180" Width = "225" DataField="Resmi" runat="server" ImageUrl='<%#"data:Image/png;base64,"+ Convert.ToBase64String((byte[])Eval("Resmi"))%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:BoundField DataField="Adi" HeaderText="Restoran Adı" />
                <asp:BoundField DataField="YemekAdi" HeaderText="Yemek Adı" />
                <asp:BoundField DataField="YemekID" HeaderText="Yemek ID" />
                <asp:BoundField DataField="HazirlanmaSuresi" HeaderText="Hazırlanma Süresi(Dk)" />
                <asp:BoundField DataField="Aciklama" HeaderText="Açıklama" /> 
                <asp:BoundField DataField="Ops1" HeaderText="Opsiyon1" />
                <asp:BoundField DataField="Ops2" HeaderText="Opsiyon2" />
                <asp:BoundField DataField="Ops3" HeaderText="Opsiyon3" />
                <asp:BoundField DataField="Fiyat" HeaderText="Fiyat(₺)" />
                <asp:BoundField DataField="SiparisID" HeaderText="SiparisID"/>
                            
            </Columns>
        </asp:GridView>
        <br />
        <asp:Label ID="Label23" runat="server" Text="&quot;Toplam tutar&quot;" Font-Bold="True"></asp:Label>
        <br />
        <br />
        <a href="MusteriIslemler.aspx">Geri Dön</a>
    
    </div>
    </form>
</body>
</html>
