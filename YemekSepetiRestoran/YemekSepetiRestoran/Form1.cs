﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace YemekSepetiRestoran
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string baglantiYolu = @"Data Source=MUSTAFA\SQLEXPRESS;Initial Catalog=YemekSepeti;Integrated Security=True";
        SqlConnection baglanti;
        SqlDataAdapter adaptor;
        SqlCommand komut;

        public string secilenRestoranAdi;
        public int secilenRestoranID;
        
        DataSet RestoranCek()
        {
            komut.CommandText = "select RestoranID,Adi from Restoran order by Adi";

            adaptor.SelectCommand = komut;
            DataSet restoranlar = new DataSet();
            adaptor.Fill(restoranlar);

            return restoranlar;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            baglanti = new SqlConnection(baglantiYolu);
            komut = new SqlCommand();
            komut.Connection = baglanti;
            adaptor = new SqlDataAdapter();

            comboBox1.ValueMember = "RestoranID";
            comboBox1.DisplayMember = "Adi";
            DataSet donenVeriler = RestoranCek();
            comboBox1.DataSource = donenVeriler.Tables[0];

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.restAd = secilenRestoranAdi;
            frm2.restID = Convert.ToString(secilenRestoranID);
            this.Hide();
            frm2.Show();
                        
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            secilenRestoranAdi = comboBox1.Text;
            secilenRestoranID = (int)comboBox1.SelectedValue;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
