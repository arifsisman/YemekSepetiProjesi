﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MusteriIslemler : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
            Label1.Text = "Merhaba, " + Session["MAd"].ToString() + " " + Session["MSoyad"].ToString();
        else
            Response.Redirect("Giris.aspx");

    }
}