﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Siparislerim : System.Web.UI.Page
{
    string baglantiYolu = ConfigurationManager.ConnectionStrings["baglantiYolum"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["MGirisYetkisi"] == true)
        {
            if (IsPostBack == false)
            {
                SqlConnection baglanti = new SqlConnection(baglantiYolu);
                SqlCommand komut = new SqlCommand();
                komut.Connection = baglanti;
                komut.CommandText = "select * from Siparis,Yemek,Restoran where Restoran.RestoranID=Yemek.RestoranID and Siparis.YemekID=Yemek.YemekID and MusteriID=" + Session["MusteriID"];
                SqlDataAdapter adaptor = new SqlDataAdapter();
                adaptor.SelectCommand = komut;
                DataSet siparis = new DataSet();
                adaptor.Fill(siparis);
                GridView1.DataSource = siparis.Tables[0];
                GridView1.DataBind();
                ToplamFiyatHesapla();
            }

        }
        else
            Response.Redirect("Giris.aspx");

    }

    void ToplamFiyatHesapla()
    {
        double toplamFiyat = 0, fiyat = 0;
        int adet;
        foreach (GridViewRow row in GridView1.Rows)
        {
            adet = Convert.ToInt32(row.Cells[0].Text);
            fiyat = Convert.ToDouble(row.Cells[10].Text);
            toplamFiyat += adet * fiyat;
        }

        Label23.Text = "Siparişlerinizin toplam tutarı= " + toplamFiyat + " Türk Lirası";
        Label23.DataBind();
    }


}