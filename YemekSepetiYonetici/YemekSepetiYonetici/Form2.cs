﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace YemekSepetiYonetici
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public Label lbl = new Label();

        string baglantiYolu = @"Data Source=MUSTAFA\SQLEXPRESS;Initial Catalog=YemekSepeti;Integrated Security=True";
        SqlConnection baglanti;
        SqlDataAdapter adaptor;
        SqlCommand komut;
        string resimYolu="";
        public bool secilenResimvar = false;

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = lbl.Text+" olarak giriş yapıldı.";

            baglanti = new SqlConnection(baglantiYolu);
            komut = new SqlCommand();
            komut.Connection = baglanti;
            adaptor = new SqlDataAdapter();
            
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {           
            Form1 frm1 = new Form1();
            frm1.Show();
        }

        void Ekle(string adi,string telefon,string adres)
        {
            FileStream fs = new FileStream(resimYolu, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] resim = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();

            komut.CommandText = "insert into Restoran values (@padi,@ptel,@presim,@padres)";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue("@padi", adi);
            komut.Parameters.AddWithValue("@ptel", telefon);
            komut.Parameters.Add("@presim", SqlDbType.Image, resim.Length).Value = resim;
            komut.Parameters.AddWithValue("@padres", adres);

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Restoran veritabanına eklendi.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
                resimYolu = "";
            }
        }

        void Guncelle(int restoranID,string adi,string tel,string adres)
        {
            if (secilenResimvar==true)
            {
                FileStream fs = new FileStream(resimYolu, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] resim = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();

                komut.CommandText = "update Restoran set Adi=@padi,Telefon=@ptel,Resmi=@presim,Adres=@padres where RestoranID=@prestoranID";
                komut.Parameters.Clear();
                komut.Parameters.AddWithValue("@prestoranID", restoranID);
                komut.Parameters.AddWithValue("@padi", adi);
                komut.Parameters.AddWithValue("@ptel", tel);
                komut.Parameters.Add("@presim", SqlDbType.Image, resim.Length).Value = resim;
                komut.Parameters.AddWithValue("@padres", adres);

            }
            else
            {
                komut.CommandText = "update Restoran set Adi=@padi,Telefon=@ptel,Adres=@padres where RestoranID=@prestoranID";
                komut.Parameters.Clear();
                komut.Parameters.AddWithValue("@prestoranID", restoranID);
                komut.Parameters.AddWithValue("@padi", adi);
                komut.Parameters.AddWithValue("@ptel", tel);
                komut.Parameters.AddWithValue("@padres", adres);
            }

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Güncelleme işlemi başarılı.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
                resimYolu = "";
                secilenResimvar = false;
            }
        }

        void Sil(int restoranID)
        {
            komut.CommandText = "delete from Restoran where RestoranID="+restoranID;

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                MessageBox.Show("Restoran silindi.");
                //KategoriSil(restoranID);//restorana ait kategorileri siler
                //YemekSil(restoranID);//restorana ait yemekleri siler
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }

        }

        void KategoriSil(int restoranID)
        {
            komut.CommandText = "delete from RestoranKategori where RestoranID=" + restoranID;

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }

        }

        void YemekSil(int restoranID)
        {
            komut.CommandText = "delete from Yemek where RestoranID=" + restoranID;

            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                baglanti.Close();
            }

        }


        DataSet RestoranAra(string adi)
        {
            komut.CommandText = "select * from Restoran where Adi like @padi+'%'";
            komut.Parameters.Clear();
            komut.Parameters.AddWithValue("@padi",adi);

            adaptor.SelectCommand=komut;
            DataSet donenVeriler=new DataSet();
            adaptor.Fill(donenVeriler);

            return donenVeriler;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files|*.jpg; *.png; *.bmp";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(ofd.FileName);
                resimYolu = ofd.FileName.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (resimYolu != "")
            {
                string adi = textBox1.Text;
                string tel = maskedTextBox1.Text;
                string adres = textBox2.Text;

                Ekle(adi, tel, adres);
            }
            else
                MessageBox.Show("Lütfen restoran resmini seçiniz.");
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            DataSet donenVeriler = RestoranAra(textBox3.Text);
            dataGridView1.DataSource = donenVeriler.Tables[0];
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "")
                textBox3.Text = " ";
            textBox3.Text = "";
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            //Düzenleme ve silme yapmak için datagridviewden satır seçmemiz gerekiyor.
            if (dataGridView1.SelectedRows.Count > 0)
            {  
                textBox6.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                textBox5.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                maskedTextBox2.Text= dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                MemoryStream ms = new MemoryStream((byte[])dataGridView1.SelectedRows[0].Cells[3].Value);
                pictureBox2.Image = Image.FromStream(ms);//streamden image okuma
                textBox4.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string gAd = textBox5.Text;
            string gTel = maskedTextBox2.Text;
            string gAdres = textBox4.Text;
            int restoranID = Convert.ToInt32(textBox6.Text);

            Guncelle(restoranID,gAd, gTel, gAdres);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files|*.jpg; *.png; *.bmp";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = Image.FromFile(ofd.FileName);
                resimYolu = ofd.FileName.ToString();
            }
            secilenResimvar = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int RestoranID = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            Sil(RestoranID);

            textBox6.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
            maskedTextBox2.Text = "";
            pictureBox2.Image = null;

        }
    }

}
