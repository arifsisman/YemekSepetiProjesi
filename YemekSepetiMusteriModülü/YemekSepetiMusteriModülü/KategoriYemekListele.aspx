﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KategoriYemekListele.aspx.cs" Inherits="KategoriYemekListele" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Yemek Listeleme</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                <asp:Label ID="LBL" Text="Kategoriye Göre Yemek Listeleme" runat="server" Font-Bold="true" ></asp:Label>
                <br />
        <br />
    Kategori Seçiniz:&nbsp<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
        <br /><br />
        <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" style="margin-right: 0px" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False">
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="LightGray" Font-Bold="True" ForeColor="Black" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
            <Columns>
                
                <asp:TemplateField HeaderText="Resmi">
                <ItemTemplate><asp:Image ID="Image1" Height = "180" Width = "225" DataField="Resmi" runat="server" ImageUrl='<%#"data:Image/png;base64,"+ Convert.ToBase64String((byte[])Eval("Resmi"))%>'></asp:Image>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:BoundField DataField="RestoranAdi" HeaderText="Restoran Adı" />
                <asp:BoundField DataField="YemekAdi" HeaderText="Yemek Adı" />
                <asp:BoundField DataField="YemekID" HeaderText="Yemek ID" />
                <asp:BoundField DataField="HazirlanmaSuresi" HeaderText="Hazırlanma Süresi(Dk)" />
                <asp:BoundField DataField="Aciklama" HeaderText="Açıklama" />
                <asp:BoundField DataField="Fiyat" HeaderText="Fiyat(₺)" />
                                <asp:TemplateField HeaderText="Yemeğe git">
                                    <ItemTemplate>
                                        <asp:Button ID="ButtonDetay" Text="Yemeğe Git" runat="server" CommandName="Detay" CommandArgument="<%# Container.DataItemIndex %>"/>
                                    </ItemTemplate>
            </asp:TemplateField>
    
            </Columns>
        </asp:GridView><br />
        <a href="MusteriIslemler.aspx">Geri Dön</a>
    </div>
    </form>
</body>
</html>
